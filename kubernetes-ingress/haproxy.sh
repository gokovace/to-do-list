#!/bin/bash

helm repo add haproxytech https://haproxytech.github.io/helm-charts
sleep 2

helm upgrade --install haproxy haproxytech/kubernetes-ingress -f haproxy-values.yaml
