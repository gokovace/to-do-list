#!/bin/bash
kubectl apply -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/v0.50.0/example/prometheus-operator-crd/monitoring.coreos.com_podmonitors.yaml
sleep 2 
kubectl apply -f cert-manager.yaml
sleep 2
kubectl apply -f podmonitor.yaml
sleep 25
kubectl apply -f issuer.yaml
